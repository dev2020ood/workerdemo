import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'webworker';
  StartWorker(){
      const worker = new Worker("./demo.worker",{type:"module"})
      worker.onmessage = (evt) => {
        console.log(evt.data);
      }
      worker.postMessage("Hello")
      console.log("Started Worker")
    }

}
